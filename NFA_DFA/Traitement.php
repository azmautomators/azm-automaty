
<?php


use Nette\Utils\Strings;


class Traitement extends Nette\Object
{
	protected $etats;
	protected $alphabet;
	protected $initials;
	protected $finals;
	const   DFA = 'DFA',
			NFA = 'NFA',
			ETAT_INITIAL = '->',
			ETAT_FINAL = '&',
			NO_TRANSITION = 'X',
			EPS = 'Epsilon',
			ETATS_DELIMITEUR = ',';



	public static function lireFichier($fichier)
	{
		$chemin = realpath($fichier);
		if (!$chemin) {
			throw new Exception("Fichier'$fichier' non trouvé");
		}

		$handle = fopen('safe://' . $chemin, 'r');
		if (!$handle) {
			throw new Exception("Impossible de lire le fichier '$fichier'.");
		}

		$static_object= new static();
		$static_object->etats = array();

		$ligne = 1;
		$headingLoaded = FALSE;

		while (!feof($handle)) {

			$parties = Strings::trim( fgets($handle) );
			if (!strlen($parties)) { 
				$ligne++;
				continue;
			}

			$parties = Strings::split( $parties, '#[\s]+#' );

			if (!$headingLoaded) {

				
				$type = array_shift($parties);
				if ($type !== static::NFA && $type !== static::DFA) {
					throw new Exception("Unexpected '$type' in '$fichier:$ligne', expected '" . static::NFA . "' or '" . static::DFA . "'.");
				}

				
				$static_object->alphabet = $parties;
				if ( count( array_unique( $static_object->alphabet ) ) !== count( $static_object->alphabet ) ) {
					throw new Exception("Symboles dupliquées dans le fichier'$fichier:$ligne'.");
				}

				$headingLoaded = TRUE;
				$ligne++;
				continue;

			}

		
			$identifier = array_shift($parties);
			$init = $final = FALSE;

			$options = preg_quote(static::ETAT_INITIAL, '#') . '|' . preg_quote(static::ETAT_FINAL, '#');
			if ($op = Strings::match($identifier, "#^(?:($options)($options)?)#")) {
				array_shift($op);

				foreach ($op as $identifiant) {
					if ($identifiant === static::ETAT_INITIAL) {
						$init = TRUE;
						$identifier = substr($identifier, strlen($identifiant));

					} elseif ($identifiant === static::ETAT_FINAL) {
						$final = TRUE;
						$identifier = substr($identifier, strlen($identifiant));
					}
				}
			}

			if (!strlen($identifier)) {
				throw new Exception("identifiant de l'état non spécifié dans '$fichier:$ligne'.");
			}

			if (!isset($etats[$identifier])) {
				$etats[$identifier] = new Etat($identifier);

			} elseif (count($etats[$identifier]->passages)) {	//transition => passage
				throw new Exception("Redéfinition de l'état $identifier' dans '$fichier:$ligne'.");
			}

			$passages = array_combine($static_object->alphabet, $parties);
			if ($passages === FALSE) {
				throw new Exception("Le nombre de passages ne correspond pas au nombre de symboles dans'$fichier:$ligne'.");
			}

			foreach ($passages as & $fin) {	//arrivees => fin
				if ($fin === static::NO_TRANSITION) {
					$fin = array();

				} else {
					$fin = explode(static::ETATS_DELIMITEUR, $fin);
					sort($fin);
					$fin = array_values( array_unique( $fin ) );

					foreach ($fin as $code => & $fc) {	//modification $symbole=> $code
						if (!isset($etats[$fc])) {	//$s => $fc
							$fc = new Etat($fc);
							$etats[$fc->identifier] = $fc;

						} else {
							$fc = $etats[$fc];
						}
					}
				}
			}

			$etats[$identifier]->setPassages( $passages )
				->setDebut( $init )
				->setFinal( $final );

			$ligne++;

		}

		$static_object->updateetats( $etats );

		if (!$static_object->determine() && $type === static::DFA) {
			trigger_error("Automate marqué comme deterministe alors qui'l est detecte non-deterministe dans le fichier : '$fichier'.", E_USER_WARNING);
		}

		return $static_object;
	}







	public function plusEpsilon()
	{
		if (($epsKey = array_search(static::EPS, $this->alphabet, TRUE)) === FALSE) {
			return $this;
		}

		foreach ($this->etats as $etat) {
			$etat->plusEpsilon();
		}

		unset($this->alphabet[$epsKey]);
		$this->updateetats();

		return $this;
	}



	
	public function determiniser()
	{
		$this->plusEpsilon();

		if ($this->determine()) {
			return $this;
		}

		$this->determEtats( $this->initials, $newetats );
		$this->updateetats( $newetats );

		return $this;
	}



	
	public function minimiser()
	{
		$this->determiniser();
		$this->plusdEtatsInaccessibles();

		$newetats = array(
			array(),
			array(),
		);

		$newPassages = array();

		
		foreach ($this->etats as $identifier => $etat) {
			$newetats[1][$identifier] = $etat->final ? '1' : '2';
		}

		while ($newetats[0] !== $newetats[1]) {
			$newetats[0] = $newetats[1];
			$newetats[1] = array();

			
			foreach ($this->etats as $identifier => $etat) {	//$state => $etat
				foreach ($etat->passages as $code => $target) {
					$newPassages[$identifier][$code] = $newetats[0][ $target[0]->identifier ];
				}
			}

			
			foreach ($this->etats as $identifier => $etat) {
			
				$found = FALSE;
				foreach ($this->etats as $i => $s) {
					if ($identifier === $i) break; 

					if ($newPassages[$identifier] === $newPassages[$i]
							&& $this->etats[$identifier]->final === $this->etats[$i]->final) {
						$found = $i;
						break;
					}
				}

				$newetats[1][$identifier] = $found === FALSE ? ( count($newetats[1]) ? (string) (max($newetats[1]) + 1) : '1' ) : (string) $newetats[1][$found];
			}
		}

		$etats = array();
		foreach ($newetats[1] as $oldidentifier => $identifier) {
			if (!isset($etats[$identifier])) {
				$etats[$identifier] = new Etat($identifier);
			}

			if ( $this->etats[$oldidentifier]->debut ) {
				$etats[$identifier]->setDebut( TRUE );
			}

			if ( $this->etats[$oldidentifier]->final ) {
				$etats[$identifier]->setFinal( TRUE );
			}

			foreach ($newPassages[$oldidentifier] as $code => & $target) {
				if (!isset($etats[$target])) {
					$tmp = $etats[$target] = new Etat($target);
					$target = array($tmp);

				} else {
					$target = array($etats[$target]);
				}
			}

			$etats[$identifier]->setPassages( $newPassages[ $oldidentifier ] );
		}

		$this->updateetats( $etats );
		return $this;
	}



	
	protected function DelEtat($identifier)
	{
		$identifier = (string) $identifier;

		if (!isset($this->etats[$identifier])) {
			throw new Exception("Impossible de supprimer l'état $identifier' - l'état n'existe pas.");
		}

		unset($this->etats[$identifier]);
		unset($this->initials[$identifier]);
		unset($this->finals[$identifier]);

		foreach ($this->etats as $etat) {
			$etat->DelEtatByidentifier($identifier);
		}

		return $this;
	}



	public function determine()
	{
		if (count($this->initials) > 1) return FALSE;

		foreach ($this->etats as $etat) {
			if (!$etat->determine()) return FALSE;
		}

		return TRUE;
	}




	public function affichage($essai = FALSE)	//affichage => affichage
	{ echo "<tr>";
		$varia = $essai
			? function ($value = '') {
				return '  ';
			}
			: function ($value = '') {
				return strlen($value) < 8 ? "\t\t\t" : (strlen($value) < 16 ? "\t\t" : "\t");
			};

		$deterministic = $this->determine();

	
		echo "<td class='vocab'>";
		echo "Etat"
			. $varia();
		echo "</td>";

		foreach ($this->alphabet as $code) {
			echo "<td class='vocab'>";
			echo $code . $varia();
			echo "</td>";

		}
		
		echo "</tr>";

		

		
		foreach ($this->etats as $identifier => $etat) {
			$etat->affichage($varia, $essai);
		}

		echo "<br/>";
		return $this;
	}



	private function updateetats(array $newetats = NULL)
	{
		if ($newetats !== NULL) {
			$this->etats = $newetats;
		}

		uasort($this->etats, 'Etat::difference');  //compare -> difference
		$this->initials = $this->finals = array();

		foreach ($this->etats as $identifier => $etat) {
			if ($etat->debut) {
				$this->initials[$identifier] = $etat;
			}

			if ($etat->final) {
				$this->finals[$identifier] = $etat;
			}
		}

		$this->validentifierer();

		return $this;
	}




	public function validentifierer()
	{
		if (!count($this->initials) || !count($this->finals)) {
			throw new Exception("Au moins un état final et un état initial.");
		}

		foreach ($this->etats as $etat) {
			if (!count($etat->passages)) {
				throw new Exception("Définition de l'état $etat' non trouvée.");
			}

			if ($etat->alphabet !== $this->alphabet) {
				throw new Exception("Le symbole de l'état $etat' ne correspond pas au vocabularire de l'automate.");
			}

			foreach ($etat->passages as $fin) {
				foreach ($fin as $p) {
					if (!isset($this->etats[$p->identifier])) {
						throw new Exception("Etat'$p' pointé par '$etat' non trouvée.");
					}
				}
			}
		}

		return $this;
	}



	
	public function plusdEtatsInaccessibles()
	{
		$this->scanaccessible( $this->initials, $accessible );
		foreach (array_diff($this->etats, $accessible) as $etat) {
			$this->DelEtat($etat->identifier);			//removeState => DelEtat
		}

		return $this;
	}



	
	private function scanaccessible(array $etats, & $accessible = NULL)
	{
		if ($accessible === NULL) {
			$accessible = array();
		}

		foreach ($etats as $etat) {
			if (!in_array($etat, $accessible, TRUE)) {
				$accessible[] = $etat;

				foreach ($etat->passages as $code => $fin) {
					$this->scanaccessible($fin, $accessible);
				}
			}
		}
	}



	private function determEtats(array $etats, & $newetats = NULL, & $processed = NULL)
	{
		if ($newetats === NULL) {
			$newetats = array();
		}

		if ($processed === NULL) {
			$processed = array();
		}

		$identifier = $this->identifierEtat($etats);

		if (!isset($newetats[$identifier])) {
			$newetats[$identifier] = new Etat($identifier);
		}

		if (!count($newetats[$identifier]->passages) && !in_array($identifier, $processed, TRUE)) {
			$processed[] = $identifier;
			$init = TRUE;
			$final = FALSE;
			$passages = array();

			foreach ($this->alphabet as $code) {
				$closure = array();
				foreach ($etats as $etat) {
					if ($init && !$etat->debut) { 
						$init = FALSE;
					}

					if (!$final && $etat->final) {
						$final = TRUE;
					}

					$closure = array_merge($closure, $etat->passages[$code]);
				}

				usort($closure, 'Etat::difference');
				$closure = array_unique($closure);

				$newidentifier = $this->identifierEtat($closure);
				if (!isset($newetats[$newidentifier])) {
					$newetats[$newidentifier] = new Etat($newidentifier);
				}

				$passages[$code] = array($newetats[$newidentifier]);

				if ($newidentifier !== $identifier) {
					$this->determEtats( $closure, $newetats, $processed );
				}
			}

			$newetats[$identifier]
					->setDebut(count($etats) && $init)
					->setFinal($final)
					->setPassages($passages);
		}
	}



	
	private function identifierEtat(array $etats)				//createidentifier() => identifierEtat()
	{
		return '{' . implode(',', $etats) . '}';
	}



	
	
}