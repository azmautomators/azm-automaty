<?php

class Etat extends Nette\Object
{

	protected $identifier;// 
	protected $debut = FALSE;//
	protected $final = FALSE;
	protected $passages = array();//
	protected $terminaison = array();//


	public function __construct($identifier)
	{
		$this->identifier = (string) $identifier;
	}

	public function __toString()
	{
		return $this->identifier;
	}

	
	public function affichage($varia, $essai = FALSE)
	{ 
		echo "<tr>";
		foreach ($this->terminaison as $index ) {
			echo $index;
			
		}

		if ($this->debut){
			if ($this->final){
				echo "<td >";
				echo "<span class='finitial'>".($initiale = Traitement::ETAT_INITIAL . Traitement::ETAT_FINAL)."</span>"; }
			else{
				echo "<td>";
				echo "<span class='finitial'>".($initiale = ' ' . Traitement::ETAT_INITIAL)."</span>" ;
			}
		}
		elseif ($this->final)
		{
			echo "<td>";
			echo "<span class='finitial'>".($initiale = ' ' . Traitement::ETAT_FINAL)."</span>";
			
		}
		else{
			echo "<td>";
			echo ($initiale = '  ');
			
		}
		
		echo $this->identifier . $varia( $initiale . $this->identifier );
		echo "</td>";

		
		foreach ($this->passages as $fin) {
			echo "<td>";
			echo ($value = (count($fin)
					? implode( Traitement::ETATS_DELIMITEUR, $fin )
					: Traitement::NO_TRANSITION ) )
				. $varia( $value );
			echo "</td>";
		}
		echo "</tr>";
		
	}



	
	public function getIdentifier()
	{
		return $this->identifier;
	}

	
	public function setIdentifier($identifier)
	{
		$this->identifier = (string) $identifier;
		return $this;
	}


	public function getDebut()
	{
		return $this->debut;
	}



	
	public function setDebut($deb = TRUE)
	{
		$this->debut = $deb;
		return $this;
	}

	
	public function getFinal()
	{
		return $this->final;
	}


	public function setFinal($ter = TRUE)
	{
		$this->final = $ter;
		return $this;
	}

	
	public function getPassages()
	{
		return $this->passages;
	}

	
	public function setPassages(array $t)
	{
		$this->passages = $t;
		return $this;
	}


	public function getAlphabet()
	{
		return array_keys($this->passages);
	}


	public function plusEpsilon()
	{
		$epsKey = Traitement::EPS;
		$this->epsilonTerminaison($this, $terminaison);

		if (count($terminaison)) {
			$passages = array();

			foreach ($terminaison as $etat) {
				if (!$this->final && $etat->final) {
					$this->final = TRUE;
				}

				foreach ($this->getAlphabet() as $code) {
					if ($code === $epsKey) continue;

					if (!isset($passages[$code])) {
						$passages[$code] = array();
					}

					$passages[$code] = array_merge($passages[$code], $etat->passages[$code]);
					usort($passages[$code], __CLASS__ . '::difference');
					$passages[$code] = array_unique( $passages[$code] );
				}
			}

			$this->passages = $passages;

		} else {
			unset($this->passages[ $epsKey ]);
		}
		$this->terminaison = $terminaison;
		return $this;
	}


	
	private function epsilonTerminaison($etat, & $terminaison = NULL)
	{
		$epsKey = Traitement::EPS;
		if ($terminaison === NULL) {
			$terminaison = array();
		}

		if (isset($etat->passages[$epsKey]) && count($etat->passages[$epsKey])) {
			if (!in_array($etat, $terminaison, TRUE)) {
				$terminaison[] = $etat;
			}

			foreach ($etat->passages[$epsKey] as $d) {
				if (!in_array($d, $terminaison, TRUE) ) {
					$terminaison[] = $d;
					$this->epsilonTerminaison($d, $terminaison);
				}
			}
		}
	}




	public function aveugle()
	{
		if ($this->final) return FALSE;

		foreach ($this->passages as $fin) {
			if (count($fin) > 1 || (count($fin) === 1 && $fin[0] !== $this))
				return FALSE;
		}

		return TRUE;
	}



	public function determine()
	{
		foreach ($this->passages as $code => $fin) {
			if ($code === Traitement::EPS || count($fin) !== 1) return FALSE;
		}

		return TRUE;
	}




	public function enleverEtat($identifier)
	{
		foreach ($this->passages as $code => $fin) {
			foreach ($fin as $index => $etat) {
				if ($etat->identifier === $identifier && $etat->identifier !== $this->identifier) {
					unset($fin[$index]);
				}
			}

			$this->passages[$code] = array_values($fin);
		}

		return $this;
	}




	public function delPassages($code, Etat $etat)
	{
		if (($index = array_search($etat, (array) $this->passages[$code], TRUE)) === FALSE) {
			throw new Exception("'$etat' n'a pas pu être retrouvé dans le passage par '$code' pour l'état '$this' ");
		}

		unset($this->passages[$code][$index]);
		return $this;
	}



	public static function difference(Etat $etat1, Etat $etat2)
	{
		if (is_numeric($etat1->identifier) && is_numeric($etat2->identifier)) {
			return (double) $etat1->identifier - (double) $etat2->identifier;

		} else {
			$result = strcmp($etat1->identifier, $etat2->identifier);

			if ($result && ($diff = strlen($etat1->identifier) - strlen($etat2->identifier) > 0)) {
				return $diff;
			}

			return $result;
		}
	}
}