<!DOCTYPE HTML>
<html>

<head>
  <title>NFA to DFA</title>
  <link rel="stylesheet" type="text/css" href="style.css" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />


</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          
          <h1>FROM NFA TO DFA</h1>
         
        </div>
      </div>
     
    </div>
   
    <div id="site_content">
      <div id="sidebar_container">
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            
            
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
        
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            
           
          </div>
          <div class="sidebar_base"></div>
        </div>
      </div>
      <div id="content">
        <?php

use Nette\Diagnostics\Debugger;
use Nette\Application\UI;
use Nette\Http;
use Nette\Http\FileUpload;

require_once __DIR__ . '/3rd-party/Nette/loader.php';
require_once __DIR__ . '/NFA_DFA/Traitement.php';
require_once __DIR__ . '/NFA_DFA/Etat.php';


?>

<br/><br/><br/><br/>

<fieldset style="padding-left: 10px; margin-left: 150px;">
<p> Pour déterminiser un automate non déterministe, veuillez joindre un fichier .txt 
  contenant votre automate, en respectant le format suivant </p>
<p align='center'><img src="exemple.png"/></p>
<ul>
  <li>Commencez votre fichier par NFA, suivi des symboles du vocabulaire séparés par un espace.</li>
  <li> Retournez à la ligne.
  <li> Pour chaque état de votre NFA, écrivez les états d'arrivée par les symboles
    en respectant l'odre dans lequel vous les avez écrits au début.
  <li>  Ajoutez <span class="finitial">« -> »</span> avant l'état initial. 
  <li>  Ajoutez <span class="finitial">« & »</span> avant chaque état final. 
  <li> Si aucune transition n'est possible, écrivez <span class="finitial">« X »</span> </li>
  <li> Si plusieurs transitions sont possibles avec le même symbole,séparez les par <span class="finitial">« , »</span> </li>
  <li>Vous pouvez à présent importer votre fichier   <form method='post' enctype='multipart/form-data'>
  <input type='file' name='fichier'></li>
  <br/>  
  <li> Valider l'automate <input type='submit' id='bouton' name='confirmer' Value="Valider" class='btn btn-info'></li>
  
  </form>
  
</ul> 
</fieldset>


<?php
if (isset($_POST['confirmer'])){

    try {
      set_time_limit(0);

      $static_object = Traitement::lireFichier( $_FILES['fichier']['tmp_name'] );

?><br/><br/>

      <h2>Automate source </h2>
<?php
      echo"<table class= 'heavyTable'border='1' style='margin: 0px auto;'>";
      echo "<br/>";
      $static_object->affichage();
      echo"</table>";
      echo "<br/>";
?>
<br/>
<hr class="style2">
      <div class="tit"><h2>Automate déterministe</h2></div>
<?php
      echo "<br/>";
      echo"<table class= 'heavyTable' border='1' style='margin: 0px auto;'>";
      $static_object->determiniser()
                    ->affichage();
        echo"</table>";

    } catch (Exception $e) {
      echo "\nError: {$e->getMessage()}\n\n";
      die();
    }
  //}
  }


?>

      </div>
    </div>
    
</body>
</html>
